# TextAnalyser

TextAnalyser ist ein Python Projekt zur analyse von wissenschaftlichen PDF Texten. 

## Getting started

`python textanalyser.py`

## Prerequisites

- Python 3 
- Package Manager pip 
    - für Linux `sudo apt install python3-pip` 
- Virtual Environment 
    - `pip3 install virtualenv` 

## Installing

1. Projekt clonen bei [BitBucket](https://bitbucket.org/wirtschaftsinformatikunijena/textanalyser.git)
2. Navigieren ins Projekt - `cd <Projektpfad>`
3. Virtuelle Environment im Projekt erstellen - `virtualenv <Beliebiger Name für Environment>`
5. Virtuelle Environment aktivieren - `source <Name der Environment>/bin/activate`
6. Requirements aus requirements.txt installieren - `pip install -r <Pfad zur requirements.txt>`

## Authors

Maximilian Nessen - maximilian.nessen@uni-jena.de


